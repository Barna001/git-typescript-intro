import { User, BattleStatistics } from "./interface";

export class IntroService {
  getIntroMessage(trainer: User, pokemonName: string): string {
    return `me:${trainer.name} trains ${pokemonName}`;
  }

  printResultOfBattle({
    me,
    mine,
    enemy,
    matchCount,
    mineWon,
  }: BattleStatistics) {
    console.log(
      `Trainer: ${me.name} had ${matchCount} fight(s) with ${
        mine.name
      } against ${enemy.name}, win percentage is ${
        (mineWon / matchCount) * 100
      }%`
    );
  }
}
