import { Pokemon, User, PokemonType } from "./interface";
import { fight } from "./arena";
import { IntroService } from "./intro";

main();

function main() {
  const introService = new IntroService();
  const trainer: User = {
    age: 12,
    name: "ASH",
  };
  const blastoise: Pokemon = {
    name: "Blastoise",
    types: [PokemonType.WATER],
  };
  const venosaur: Pokemon = {
    name: "Venosaur",
    types: [PokemonType.GRASS, PokemonType.POISON],
  };

  console.log(introService.getIntroMessage(trainer, venosaur.name));

  const matchCount = 100;
  const venosaurWinCount = getWinCount(matchCount, venosaur, blastoise);

  introService.printResultOfBattle({
    me: trainer,
    mine: venosaur,
    enemy: blastoise,
    matchCount,
    mineWon: venosaurWinCount,
  });
}

function getWinCount(
  numberOfMatches: number,
  myPokemon: Pokemon,
  enemyPokemon: Pokemon
): number {
  let matchesWon = 0;
  for (let i = 0; i < numberOfMatches; i++) {
    if (fight(myPokemon, enemyPokemon)) {
      matchesWon++;
    }
  }
  return matchesWon;
}
