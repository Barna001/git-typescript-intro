export interface User {
  name: string;
  age: number;
  height?: number;
}

export enum PokemonType {
  FIRE = "fire",
  WATER = "water",
  GRASS = "grass",
  POISON = "poison",
}

export interface Pokemon {
  name: string;
  types: PokemonType[];
}

export interface BattleStatistics {
  me: User;
  matchCount: number;
  mine: Pokemon;
  enemy: Pokemon;
  mineWon: number;
}
