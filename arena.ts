import { Pokemon, PokemonType } from "./interface";

interface TypeAdvantage {
  stronger: PokemonType;
  weaker: PokemonType;
}

const typeAdvantages: TypeAdvantage[] = [
  {
    stronger: PokemonType.FIRE,
    weaker: PokemonType.GRASS,
  },
  {
    stronger: PokemonType.WATER,
    weaker: PokemonType.FIRE,
  },
  {
    stronger: PokemonType.GRASS,
    weaker: PokemonType.WATER,
  },
];

// simulates if pokemon 1 defeats pokemon 2
export function fight(pokemon1: Pokemon, pokemon2: Pokemon): boolean {
  const highChance = 0.7;
  const normalChance = 0.5;
  const random = Math.random();

  // forEach cannot be used here with this implementation, because the inner return would not return from the fight function
  for (const typeAdvantage of typeAdvantages) {
    for (const type1 of pokemon1.types) {
      for (const type2 of pokemon2.types) {
        if (typeStrongerThan(type1, type2, typeAdvantage)) {
          return random < highChance;
        }
        if (typeStrongerThan(type2, type1, typeAdvantage)) {
          return random >= highChance;
        }
      }
    }
  }
  return random < normalChance;
}

function typeStrongerThan(
  type: PokemonType,
  compare: PokemonType,
  advantage: TypeAdvantage
): boolean {
  return type === advantage.stronger && compare === advantage.weaker;
}
